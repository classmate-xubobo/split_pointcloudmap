cmake_minimum_required(VERSION 3.0.2)
project(pcd_split)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++14)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  roscpp
  pcl_conversions
  sensor_msgs
)

## Specify additional places of header files
include_directories(
  ${catkin_INCLUDE_DIRS}
  include
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES pcd_split
  CATKIN_DEPENDS roscpp pcl_conversions sensor_msgs
#  DEPENDS system_lib
)

add_executable(${PROJECT_NAME} src/split_map.cpp)

## Specify libraries to link a library or executable target against
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)
