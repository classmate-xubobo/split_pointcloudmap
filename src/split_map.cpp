#include <iostream>
#include <fstream>
#include <sstream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <ros/ros.h>
#include <vector>

class PCDSplit
{
public:
    ros::NodeHandle nh;

    std::string PROJECT_NAME;
    std::string read_pcdfile;
    std::string read_pcdname;
    std::string save_pcdfile;
    std::string save_pcdname;
    std::string MaptxtDirectory;
    std::vector<std::vector<int>> map_data_txt;

    PCDSplit()
    {
        nh.param<std::string>("/PROJECT_NAME", PROJECT_NAME, "split_map");
        nh.param<std::string>(PROJECT_NAME + "/read_pcdfile", read_pcdfile, "/python/");
        nh.param<std::string>(PROJECT_NAME + "/read_pcdname", read_pcdname, "cloudGlobal");
        nh.param<std::string>(PROJECT_NAME + "/save_pcdfile", save_pcdfile, "/python/map/");
        nh.param<std::string>(PROJECT_NAME + "/save_pcdname", save_pcdname, "cloudGlobal");
        nh.param<std::string>(PROJECT_NAME + "/MaptxtDirectory", MaptxtDirectory, "/python/");
    }

    void Readmaptxt() {
        std::ifstream map_file(std::getenv("HOME") + MaptxtDirectory + "map_file.txt");

        if (!map_file.is_open()) {
            ROS_ERROR("Unable to open map_file!");
            return;
        }

        std::string line;
        while (std::getline(map_file, line)) {
            std::vector<int> row;

            std::stringstream ss(line);
            int number;
            while (ss >> number) {
                row.push_back(number);
            }

            map_data_txt.push_back(row);
        }

        map_file.close();
    }


    void processPCD()
    {
        pcl::PointCloud<pcl::PointXYZI>::Ptr MapCornerPoint(new pcl::PointCloud<pcl::PointXYZI>());
        pcl::io::loadPCDFile(std::getenv("HOME") + read_pcdfile + read_pcdname + ".pcd", *MapCornerPoint); // read pcd

        if (MapCornerPoint->points.size() < 100) {
            ROS_ERROR("MAP POINTSCLOUD LOAD ERROR!");
            ros::shutdown();
        }

        Readmaptxt();

        int map_number = map_data_txt.size();
        std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> mapCornerVector;


        for(int i = 0; i < map_number; i++) {
            int m = map_data_txt[i][4];
            int n = map_data_txt[i][5];
            pcl::PointCloud<pcl::PointXYZI>::Ptr MapCorner_rc(new pcl::PointCloud<pcl::PointXYZI>());
            mapCornerVector.push_back(MapCorner_rc);
        }

        for (pcl::PointXYZI& point : MapCornerPoint->points) {
            float x = point.x;
            float y = point.y;

            for (int j = 0; j < map_number; j++) {
                double x1 = map_data_txt[j][0], y1 = map_data_txt[j][1], x2 = map_data_txt[j][2], y2 = map_data_txt[j][3];
                if (x >= x1 && x < x2 && y >= y1 && y < y2) {
                    mapCornerVector[j]->push_back(point);

                    break;
                }

            }
        }

        for (int i = 0; i <map_number; i++) {
            if (mapCornerVector[i]->size() == 0) {
                continue;
            }
            pcl::io::savePCDFileASCII(std::getenv("HOME") + save_pcdfile + save_pcdname + "_" + std::to_string(map_data_txt[i][4]) +
             "_" + std::to_string(map_data_txt[i][5]) + ".pcd", *mapCornerVector[i]);
        }

        ROS_INFO("\033[32mSPLIT MAP SUCCESS!\033[0m");


        ros::shutdown();
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "pcd_split");
    PCDSplit pcdsplit;
    ros::Rate loop_rate(10);
    while (ros::ok())
    {
        pcdsplit.processPCD();
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}